const keys = document.querySelectorAll('.key');

keys.forEach(key => {
    key.addEventListener('click', () => {
        keyHandler(key.id);
    });
});

document.addEventListener('keydown', event => {
    console.log(event);
    const keyName = event.key.toUpperCase();
    const targetKey = document.getElementById('key' + keyName);
    console.log(targetKey);
    if (targetKey) {
        keyHandler(targetKey.id);
    }
});

function keyHandler(keyId) {
    keys.forEach(key => {
        if (key.id === keyId) {
            key.style.backgroundColor = 'blue';
        } else {
            key.style.backgroundColor = '';
        }
    });
};

